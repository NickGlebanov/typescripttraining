// Компиляция и базовое написание
function getFinalPrice(price: number, discount: number) {
    return price - price / discount;
}

console.log(getFinalPrice(100, 10));

// типы

type Foot = number;
type Pound = number;

type Patient = {
    name: string;
    height: Foot;
    weight?: Pound; // опциональное свойство
}

let patient: Patient = {
    name: 'Joe Smith',
    height: 5,
    weight: 100
}

patient.height = 3;


// интерфейсы
interface Rectangle {
    kind: "rectangle";
    width: number;
    height: number;
}

interface Circle {
    kind: "circle";
    radius: number;
}

type Shape = Rectangle | Circle;

function area(shape: Shape): number {
    switch (shape.kind) {
        case "rectangle":
            return shape.height * shape.width;
        case "circle":
            return Math.PI * shape.radius ** 2;
    }
}

const myRectangle: Rectangle = {kind: "rectangle", width: 10, height: 20};
console.log(`Rectangle's area is ${area(myRectangle)}`);

const myCircle: Circle = {kind: "circle", radius: 10};
console.log(`Circle's area is ${area(myCircle)}`);

// деструктуризация

type User = {
    firstName: string;
    age: number;
}

function foo({firstName, age}: User) {
    console.log(firstName, age);
}

let user: User = {
    firstName: 'John',
    age: 4
}

foo(user)
foo({firstName: "john", age: 43})

// классы

class Parent {

    readonly index: number = 4; // инициализировано в определении класса
    readonly hash: number; // инициализировано в конструкторе
    constructor(firstarg: number, secondarg: number) {
        this.hash = firstarg * secondarg;
    }

    sayArgs(arg1: string): string {
        return arg1 + this.hash;
    }

}

const parentObj: Parent = new Parent(1, 2)
console.log(parentObj.sayArgs("hash = "))

// будет ошибка так как поле readonly
//parentObj.index = 4

class Child extends Parent {
    readonly childArg: string = "sddsds";
    constructor() {
        super(4,4);
    }
}

const childObj = new Child();
console.log(childObj.childArg)
console.log(childObj.hash)